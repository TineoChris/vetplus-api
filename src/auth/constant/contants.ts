export const jwtConstant = {
  secret: 'Hbk13Adj@m8491vhdie2',
};

export enum SignUpResult {
  COMPLETED = 'COMPLETED',
  PENDING = 'PENDING',
}

export enum SignUpMessage {
  USER_CREATED = 'USER_CREATED',
  USER_IN_PENDING = 'USER_IN_PENDING',
}
